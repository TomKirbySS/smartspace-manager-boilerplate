﻿using System.Runtime.Serialization;
using ConnectLabs.Connect.Interface.Trunking;

namespace ConnectLabs.XXSSXX.Manager.Service.Contract
{
    [DataContract(Namespace = "ConnectLabs.XXSSXX.Manager.Service.Contract")]
    [KnownType(typeof(ListAllColoursResponse))]
    public class ViewResponse : Response
    {
        protected ViewResponse(Version version) : base(version)
        {
        }
    }
}