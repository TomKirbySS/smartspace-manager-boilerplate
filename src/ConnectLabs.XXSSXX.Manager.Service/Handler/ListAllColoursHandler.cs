﻿using System;
using System.Collections.Generic;
using ConnectLabs.Connect.Interface.Trunking;
using ConnectLabs.XXSSXX.Manager.Service.Contract;
using ConnectLabs.XXSSXX.Manager.Service.Contract.DataObjects;
using ConnectLabs.XXSSXX.Manager.Service.Policy;

namespace ConnectLabs.XXSSXX.Manager.Service.Handler
{
    [RequestHandler]
    public class ListAllColoursHandler
    {
        private readonly IListAllColoursPolicy _policy;

        public ListAllColoursHandler(IListAllColoursPolicy policy)
        {
            _policy = policy ?? throw new ArgumentNullException(nameof(policy));
        }

        public ListAllColoursResponse View(ListAllColoursRequest request)
        {
            
            
            List<Colour> colorList = new List<Colour>();
            colorList.Add(new Colour(){ Title = "Green"});
            colorList.Add(new Colour(){ Title = "Red"});
            colorList.Add(new Colour(){ Title = "Blue"});
            colorList.Add(new Colour(){ Title = "White"});
            colorList.Add(new Colour(){ Title = "Black"});

            return new ListAllColoursResponse(){ Colours = colorList};
        }
    }
}