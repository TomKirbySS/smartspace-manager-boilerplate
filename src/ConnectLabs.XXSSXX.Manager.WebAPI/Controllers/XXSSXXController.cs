﻿using System;
using ConnectLabs.Connect.Interface.Trunking;
using ConnectLabs.Connect.Mvc.Middleware.Authentication;
using ConnectLabs.XXSSXX.Manager.Service.Contract;
using Microsoft.AspNetCore.Mvc;

namespace ConnectLabs.XXSSXX.Manager.WebAPI
{
    [Produces("application/json")]
    [Route("api/XXSSXX")]
    [Microsoft.AspNetCore.Authorization.Authorize(ActiveAuthenticationSchemes = ConnectAuthenticationOptions.AuthenticationSchemes)]
    public class XXSSXXController : Controller
    {
        private readonly IRequestRouter _router;

        public XXSSXXController(IRequestRouter router)
        {
            _router = router ?? throw new ArgumentNullException(nameof(router));
        }

        private ViewResponse View(ViewRequest request)
        {
            return (ViewResponse) _router.Handle(nameof(View), request);
        }

        [HttpPost]
        [Route("View/ListAllColours")]
        public ListAllColoursResponse ListAllColours([FromBody] ListAllColoursRequest request)
        {
            return (ListAllColoursResponse)View(request);
        }
    }
}