﻿using System.Runtime.Serialization;
using ConnectLabs.Connect.Interface.Trunking;

namespace ConnectLabs.XXSSXX.Manager.Service.Contract
{
    [DataContract(Namespace = "ConnectLabs.XXSSXX.Manager.Service.Contract")]
    [KnownType(typeof(ListAllColoursRequest))]
    public class ViewRequest : Request
    {
        protected ViewRequest(Version version) : base(version)
        {
        }
    }
}