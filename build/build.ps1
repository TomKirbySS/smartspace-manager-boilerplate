<#
.SYNOPSIS
Build script for TeamCity.

.DESCRIPTION
Build file based on Generic Build file for Building Utility Projects and Publishing to NuGet.
It is expected to be ran in TeamCity, therefore includes specific TeamCity outputs within the script.

.PARAMETER buildNumber
The build number for this build.

.PARAMETER isDefaultBranch
Indicates if this it this is the default (master) branch of not. (Expects string of "true" or "false" in lowercase).

.PARAMETER dotcoverCmd
The full filename for the dotcover executable.

.PARAMETER msbuildCmd
The full filename for the msbuild executable.

.PARAMETER octopusDeployPackageRepositoryUrl
The url of the octopus deploy

.PARAMETER octopusDeployTeamCityUserApiKey
The API Key for the user with permission to create and deploy releases on Octopus

.PARAMETER octopusProjectName
The name of the octopus project this package is a release for.

.PARAMETER $octopusDeployTarget
The name of the target environment to deploy the release too.
#>
param(
	[Parameter(Mandatory=$True)][string]$buildNumber,
	[Parameter(Mandatory=$True)][string]$isDefaultBranch,
	[Parameter(Mandatory=$True)][string]$msbuildCmd,
	[Parameter(Mandatory=$True)][string]$octopusDeployPackageRepositoryUrl,
	[Parameter(Mandatory=$True)][string]$octopusDeployTeamCityUserApiKey,
	[Parameter(Mandatory=$True)][string]$octopusProjectName,
	[Parameter(Mandatory=$True)][string]$octopusDeployTarget,
    [Parameter(Mandatory=$True)][string]$bitbucketUser,
    [Parameter(Mandatory=$True)][string]$createRelease,
	[Parameter(Mandatory=$True)][string]$doDeploy,
	[Parameter(Mandatory=$False)][string]$buildScript = "Build\ServiceFabric\buildMultiTarget.ps1",
    [Parameter(Mandatory=$False)][string]$doTests = "true"
)

try
{
	$cloneScript = Join-Path $PSScriptRoot "cloneRepo.ps1"
	$targetDirectory = ".\CD\BuildTools"
	
	Write-Output "Clone script location: $cloneScript"	
	& "$cloneScript" -repoName "cdtools" -targetDirectory "$targetDirectory" -bitbucketUser $bitbucketUser

	Write-Output "Executing build script."
	$buildScript = Join-Path -Path $targetDirectory -ChildPath "$buildScript"
	& "$buildScript" -buildNumber $buildNumber `
		-isDefaultBranch $isDefaultBranch `
		-msbuildCmd $msbuildCmd `
		-octopusDeployPackageRepositoryUrl $octopusDeployPackageRepositoryUrl `
		-octopusDeployTeamCityUserApiKey $octopusDeployTeamCityUserApiKey `
		-octopusProjectName $octopusProjectName `
		-octopusDeployTarget $octopusDeployTarget `
		-createRelease $createRelease `
		-doDeploy $doDeploy `
		-doTests $doTests
}
catch
{
	Write-Output "Something went wrong."
    Write-Output $_.Exception.Message
	exit -1
}