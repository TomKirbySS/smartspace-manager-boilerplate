﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ConnectLabs.XXSSXX.Manager.Service.Contract.DataObjects
{
    public class Colour
    {
        [DataMember] public string Title { get; set; }
    }
}
