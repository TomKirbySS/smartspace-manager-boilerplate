﻿namespace ConnectLabs.XXSSXX.Manager.Service.Contract
{
    public interface IXXSSXXManager
    {
        ViewResponse View(ViewRequest request);
    }
}