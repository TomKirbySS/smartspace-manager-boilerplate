﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using ConnectLabs.Connect.Interface.Trunking;
using ConnectLabs.XXSSXX.Manager.Service.Contract.DataObjects;
using Version = ConnectLabs.Connect.Interface.Trunking.Version;

namespace ConnectLabs.XXSSXX.Manager.Service.Contract
{
    [DataContract(Namespace = "ConnectLabs.XXSSXX.Manager.Service.Contract")]
    public class ListAllColoursResponse : ViewResponse
    {
        private List<Colour> _colours;

        public ListAllColoursResponse() : base(new Version(1))
        {
        }

        [DataMember]
        public List<Colour> Colours
        {
            get => _colours ?? (_colours = new List<Colour>());
            set => _colours = value;
        }
    }
}