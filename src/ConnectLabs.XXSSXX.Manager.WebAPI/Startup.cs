﻿using ConnectLabs.Connect.Mvc;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;
using System.Collections.Generic;
using System.Fabric;
using System.Linq;
using ConnectLabs.XXSSXX.Manager.Service;
using ConnectLabs.XXSSXX.Manager.Service.Contract;

namespace ConnectLabs.XXSSXX.Manager.WebAPI
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)

                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            // Add Connect Mvc pipeline
            services.AddConnectMvc<IXXSSXXManager>(settings =>
            {
                settings.ProvideDefinition = false;
                settings.PlatformSettings = new PlatformSettings(GetPlatformSettings());
            });

            // Load settings
            Settings localSettings = LoadSettings();
            services.AddSingleton(localSettings);

            // Add connect services
            services.AddConnectService<IXXSSXXManager>();

            // Add proxies

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Mobile Workplace App Manager", Version = "v1" });
            });
        }

        private static System.Fabric.Description.ConfigurationSettings FabricConfiguration => FabricRuntime.GetActivationContext().GetConfigurationPackageObject("Config").Settings;

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseConnectMvc(builder =>
            {
                builder.UseCors()
                       .UseAuthentication(authConfig =>
                       {
                           authConfig.Issuer = FabricConfiguration.Sections["Platform"].Parameters["IdentityServerUrl"].Value;
                       });
            });

            app.UseSwagger();
            app.UseSwaggerUI(c => { c.SwaggerEndpoint("v1/swagger.json", "XXSSXX Manager"); });
        }

        public static IDictionary<string, string> GetPlatformSettings()
        {
            return FabricRuntime.GetActivationContext()
                .GetConfigurationPackageObject("Config")
                .Settings.Sections["Platform"]
                .Parameters.ToDictionary(k => k.Name, v => v.Value);
        }

        private Settings LoadSettings()
        {
            var config = FabricRuntime.GetActivationContext().GetConfigurationPackageObject("Config").Settings;
            var externalEndpointSettings = config.Sections["ExternalEndpoints"];

            var settings = new Settings
            {
            };

            return settings;
        }
    }
}
