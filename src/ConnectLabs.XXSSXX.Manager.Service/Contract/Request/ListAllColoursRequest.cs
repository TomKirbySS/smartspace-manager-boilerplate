﻿using System.Runtime.Serialization;
using ConnectLabs.Connect.Interface.Trunking;
using Version = ConnectLabs.Connect.Interface.Trunking.Version;

namespace ConnectLabs.XXSSXX.Manager.Service.Contract
{
    [DataContract(Namespace = "ConnectLabs.XXSSXX.Manager.Service.Contract")]
    public class ListAllColoursRequest : ViewRequest
    {
        public ListAllColoursRequest() : base(new Version(1))
        {
        }
    }
}